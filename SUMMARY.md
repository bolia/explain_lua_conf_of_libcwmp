# Summary

* [序](README.md)
* [0. lua和hook](0.md)
* [1. 一共8个钩子、两个脚本](1.md)
  * [1.1 LoadCfgMap](1/11-loadcfg.md)
  * [1.2 XMLPathMap](1/12-xmlpathmap.md)
  * [1.3 SetparamMap](1/13-setparammap.md)
  * [1.4 Node\_SetValue](1/14-nodesetvalue.md)
  * [1.5 MAP\_SOAPTag](1/15-mapsoaptag.md)
  * [1.6 Node\_GetValue](1/16-nodegetvalue.md)
  * [1.7 EventCode](1/17-eventcode.md)
  * [1.8 evtPara](1/18-evtpara.md)
* [2. lua的一些语法](2.md)

