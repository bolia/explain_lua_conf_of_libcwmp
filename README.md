# 本文目的

本文旨在介绍libcwmp的lua配置文件的使用、修改方法。

# 作者

cdx

# 背景

  cwmp既TR069，也就是我们俗称的网管。这是一个协议，C/S架构，描述了Client和Server如何配合以实现一些功能如ping、升级固件等。我们我们这里说的网管代指网管Client终端，也就是在STB运行的程序。

  一般，商用ACS（网管Server端，Atuo-Config-Server）差别不是很大，流程上是基本相同的，各服务器间不同的点于在上报参数列表中的条目和数量。如果把每种Inform（“1 Boot”、"2 Period"、etc）需要的上报的条目都分别列写入配置文件，如果 在上报之前有个脚本能对上报的数据查漏补缺的话就能在不修改代码（那么就不需要编译）的情况通配各家服务器。

  于是，这份文档也就应运而生了，因为我实现了这个想法，文档会说明脚本用途。会告诉你，什么时候脚本会被调用，返回什么，返回结果影响着什么。

