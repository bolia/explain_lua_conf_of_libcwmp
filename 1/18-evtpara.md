# ParamListMap

```
--要额外添加的节点，加到下面
ParamListMap = {
    "Device.ManagementServer.URL",
    "Device.X_00E0FC.STBID",
    "Device.LAN.MACAddress",
}
```

这里成员是随便添加的，形式就是tr069的节点路径形式。当然，形式可以不同，只是其它是否有意义的问题而已

**效果：**

`这里填什么，上报的信息就一定有什么`

`也就是说，我们可以使用这个数组达到强制添加一些节点到每次上报的内容当中`

