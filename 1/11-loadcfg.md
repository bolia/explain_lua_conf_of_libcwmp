# LoadCfgMap

```
--load cfg
LoadCfgMap = {
    ['office']                   =  "GDYDX",
    ['zeroTouchUseEventID']      =  "0",
}
```

目前只有office、zeroTouchUseEventID这两个key

office是`局点代码`

zeroTouchUseEventID是`零配置使用的EvnetCode编码`

**举个例子：**

`当修改zeroTouchUseEventID的值为1的时候，零配置的时候就会上报“1 Boot”`

