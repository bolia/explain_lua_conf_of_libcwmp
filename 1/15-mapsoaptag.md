# MAP\_SOAPTag

```
-- SOAP tag map
MAP_SOAPTag = {
    -- default --
    -- ["GET_PARAM_TYPE_EX"] =        "xsd:string",
    -- ["GET_PARAM_ATTR_TYPE_EX"] =   "xsd:string",
    -- ["SET_PARAM_ATTR_TYPE_EX"] =   "xsd:string",

    -- huawei --
    ["GET_PARAM_TYPE_EX"] =      "SOAP-ENC:xsd:string",
    ["GET_PARAM_ATTR_TYPE_EX"] = "cwmp:string",
    ["SET_PARAM_ATTR_TYPE_EX"] = "cwmp:string",

    -- Liang Chuang --
    -- ["GET_PARAM_TYPE_EX"] =      "cwmp:ParameterValueStruct",
    -- ["GET_PARAM_ATTR_TYPE_EX"] = "cwmp:ParameterValueStruct",
    -- ["SET_PARAM_ATTR_TYPE_EX"] = "xsd:string",

    -- CMCC-HangZhou research institute --
    -- ["GET_PARAM_TYPE_EX"] =      "xsd:string",
    -- ["GET_PARAM_ATTR_TYPE_EX"] = "",
    -- ["SET_PARAM_ATTR_TYPE_EX"] = "xsd:string",

    ["end"] = "end"
}
```

目前只有GET\_PARAM\_TYPE\_EX、GET\_PARAM\_ATTR\_TYPE\_EX、SET\_PARAM\_ATTR\_TYPE\_EX这三个key

GET\_PARAM\_TYPE\_EX是GetParameterValues时使用的解析TAG

GET\_PARAM\_ATTR\_TYPE\_EX是GetParameterAttrs时使用的解析TAG

SET\_PARAM\_ATTR\_TYPE\_EX是SetParameterAttrs时使用的解析TAG

**举个例子：**

`none`

