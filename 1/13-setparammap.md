# SetparamMap

```
--服务器设置值
SetparamMap = {
    --["Device.ManagementServer.URLModifyFlag"]     =   "14",
}
```

这里的key 是随便添加的，key的形式就是tr069的节点路径形式。当然，形式可以不同，只是其它是否有意义的问题而已

**举个例子：**

`当修改Device.ManagementServer.URLModifyFlag的值为14的时候，`**`不管ACS怎么下发这个值，都会在这里强制改为14`**

