# Node\_SetValue

```
--盒子自身获取值（setValue）
Node_SetValue = {
    ["Device.X_00E0FC.ServiceInfo.UserID"]     =   "0123456789",
}
```

这里的key 是随便添加的，key的形式就是tr069的节点路径形式。当然，形式可以不同，只是其它是否有意义的问题而已

**举个例子：**

`当修改Device.X_00E0FC.ServiceInfo.UserID的值为0123456789的时候，`**`不管ACS怎么下发这个值，用户怎么修改（设置）都会在这里强制改为0123456789`**

