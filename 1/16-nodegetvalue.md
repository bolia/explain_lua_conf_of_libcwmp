# Node\_GetValue

```
--盒子自身获取值（getValue）
Node_GetValue = {
    ["Device.ManagementServer.URL"]                  =   "http://125.88.84.6:37021/acs",
    ["Device.ManagementServer.Username"]             =   "testcpe",
    ["Device.ManagementServer.Password"]             =   "ac5entry",
    ["Device.ManagementServer.ConnectionRequestURL"] =   "http://192.168.0.189:6547",
    ["Device.ManagementServer.ParameterKey"]         =   "123456",
    ["Device.ManagementServer.PeriodicInformInterval"]   =   "3600",

    ["Device.DeviceInfo.ModelName"]                  =   "FIBERHOME_HG680",
    ["Device.DeviceInfo.Description"]                =   "HuaweiIPSTB",
    ["Device.DeviceInfo.FirstUseDate"]               =   "2014-03-07T17:00:31",
    ["Device.DeviceInfo.HardwareVersion"]            =   "HG680-JD89H-52",
    ["Device.DeviceInfo.SoftwareVersion"]            =   "1.8.0-D-20160628.1103",
    ["Device.DeviceInfo.AdditionalHardwareVersion"]  =   "1.8.0-D-20160628.1103",
    ["Device.DeviceInfo.AdditionalSoftwareVersion"]  =   "1.8.0-D-20160628.1103",
    ["Device.DeviceInfo.Manufacturer"]               =   "Fiberhome",
    ["Device.DeviceInfo.ManufacturerOUI"]            =   "00E0FC",
    ["Device.DeviceInfo.ProductClass"]               =   "FIBERHOME_HG680",
    ["Device.DeviceInfo.SerialNumber"]               =   "48:55:5F:FF:E0:8B",

    ["Device.LAN.IPAddress"]                         =   "192.168.0.189",
    ["Device.LAN.MACAddress"]                        =   "48:55:5F:FF:E0:8B",

    ["Device.X_00E0FC.STBID"]                        =   "0010039900E06800SYXX48555FFFE08B",
    ["Device.X_00E0FC.ServiceInfo.UserID"]           =   "080820140904999",
    ["Device.X_00E0FC.ServiceInfo.AuthURL"]          =   "http://eds.iptv.gd.cn:8082/EDS/jsp/AuthenticationURL",
    ["Device.X_00E0FC.ServiceInfo.PPPoEID"]          =   "gdiptv@iptv.gd",
    --["Device.X_00E0FC.ServiceInfo.IsEncryptMark"]    =   "1",
    ["Device.X_00E0FC.ErrorCodeSwitch"]              =   "1"

}
```

这里的key 是随便添加的，key的形式就是tr069的节点路径形式。当然，形式可以不同，只是其它是否有意义的问题而已

**举个例子：**

`当修改Device.ManagementServer.URL的值为http://125.88.84.6:37021/acs的时候，不管数据是来自java数据库还是系统的prop系统还是来自文件，甚至根本不存在的一个key。只要这里定义了，上报时都是按照这里填写的数据开上报的。`

**`也就是说，用这个数据用构造任意你想要模仿的厂商信息，包括任意节点个数，节点值`**

