# XMLPathMap

```
--xml path
XMLPathMap = {
    ['custom']  =  "/system/etc/DataModel.xml",
}
```

目前只有custom这个key

**举个例子：**

`当修改custom的值为“/data/123.xml”的时候，xml解析的时候就会使用"/data/123.xml"作为目标文件路径去加载`

